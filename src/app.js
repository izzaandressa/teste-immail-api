const express = require('express');
const mongoose = require('mongoose');
const Games = require('./models/games');

const fs = require('fs');
const LogParser = require('../LogParser');

require('dotenv').config();

const app = express();

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.use(express.static(__dirname + '/public'));

mongoose.connect(process.env.DATABASE_CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

db.on('connected', () => {
    console.log('Conexão com mongoose OK!');

    db.dropDatabase((err, success) => {
        err && console.log("Error : " + err);
        if (err) throw err;
        success && console.log("Limpando jogos.");

        fs.readFile('./games.log', (err, logData) => {
            if (err) {
                console.log(err);
            }

            let file = logData.toString();
            let parserRef = new LogParser();
            let gameList = parserRef.parse(file);

            try {
                gameList.map(game => {
                    const gameRef = new Games({
                        total_kills: game.total_kills,
                        players: game.players,
                        kills: game.kills,
                        duration_game: game.duration_game,
                        kills_by_means: game.kills_by_means
                    })
                    gameRef.save();
                })

                console.log('Jogos cadastrados com sucesso!');
            } catch (e) {
                console.log('Falha ao cadastrar jogos.');
            }

        });
    });
});

db.on('error', err => {
    console.log(`Erro de conexão mongoose: \n${err}`);
});

db.on('disconnected', () => {
    console.log('Conexão com mongoose perdida.');
});

process.on('SIGINT', () => {
    db.close(() => {
        console.log(
            'Conexão com mongoose perdida, aplicação finalizada.'
        );
        process.exit(0);
    });
});


const indexRoutes = require('./routes/index-routes');
app.use('/', indexRoutes);

const gamesRoutes = require('./routes/games-routes');
app.use('/games', gamesRoutes);

module.exports = app;