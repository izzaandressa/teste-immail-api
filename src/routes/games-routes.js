const express = require('express');
const router = express.Router();
const gamesController = require('../controllers/games-controller');

router.get('/', gamesController.listGames);

module.exports = router;