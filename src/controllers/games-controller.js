const mongoose = require('mongoose');
const Games = mongoose.model('Games');

exports.listGames = async (req, res) => {
    try {
        const data = await Games.find({}, "total_kills players kills duration_game kills_by_means -_id");
        // console.log(data);
        res.status(200).send(data);
    } catch (e) {
        res.status(500).send({ message: 'Falha ao carregar os jogos.' });
    };
}