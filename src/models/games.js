const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    total_kills: {
        type: Number,
        required: false
    },
    players: {
        type: Array,
        required: true
    },
    kills: {
        type: Object,
        required: false
    },
    duration_game: {
        type: String,
        required: false
    },
    kills_by_means: {
        type: Object,
        required: false
    }
});

module.exports = mongoose.model('Games', schema);