'use strict';

const LogParser = function () { };

LogParser.prototype.parse = (file) => {

  const meansOfDeathEnum = {
    MOD_UNKNOWN: "Morte por motivo desconhecido",
    MOD_SHOTGUN: "Morte por espingarda",
    MOD_GAUNTLET: "Morte por gauntlet",
    MOD_MACHINEGUN: "Morte por uma metralhadora",
    MOD_GRENADE: "Morte por uma granada",
    MOD_GRENADE_SPLASH: "Morte por uma granada splash",
    MOD_ROCKET: "Morte por um foguete",
    MOD_ROCKET_SPLASH: "Morte por um foguete splash",
    MOD_PLASMA: "Morte por um plasma",
    MOD_PLASMA_SPLASH: "Morte por um plasma splash",
    MOD_RAILGUN: "Morte por uma arma ferroviária",
    MOD_LIGHTNING: "Morte por um relâmpago",
    MOD_BFG: "Morte por bfg",
    MOD_BFG_SPLASH: "Morte por bfg splash",
    MOD_WATER: "Morte por falta de água",
    MOD_SLIME: "Morte por slime",
    MOD_LAVA: "Morte por lava",
    MOD_CRUSH: "Morte por paixão súbita",
    MOD_TELEFRAG: "Morte por telefrago",
    MOD_FALLING: "Morte por queda",
    MOD_SUICIDE: "Morte cometeu suicídio",
    MOD_TARGET_LASER: "Morte por ser alvo de lazer",
    MOD_TRIGGER_HURT: "Morte por um gatilho de dor",
    MISSIONPACK: "Morte em uma missão pack",
    MOD_NAIL: "Morte pela unha",
    MOD_CHAINGUN: "Morte por armação de corrente",
    MOD_PROXIMITY_MINE: "Morte por uma mina de proximidade",
    MOD_KAMIKAZE: "Morte por um ataque kamikaze",
    MOD_JUICED: "Morte espremido",
    MOD_GRAPPLE: "Morte agarrarado"
  }

  let games = [];
  const allLines = file.split('\n');

  allLines.map(line => {
    const listLine = line.trim().split(' ');

    if (listLine[1] === 'InitGame:') {
      newGame();
    } else if (listLine[1] === 'ShutdownGame:') {
      finishGame();
    } else if (listLine[1] === 'ClientUserinfoChanged:') {
      addUser();
    } else if (listLine[1] === 'Kill:') {
      getKillList();
    } else {
      return false;
    }

    function newGame() {
      games.push({
        total_kills: 0,
        players: [],
        kills: {},
        duration_game: "",
        kills_by_means: {}
      });
      games[games.length - 1].push + 1;
    }

    function finishGame() {
      const regexHour = /(?:(?:(?<hh>\d{1,2})[:.])?(?<mm>\d{1,2})[:.])?(?<ss>\d{1,2})/;
      const hourValue = line.match(regexHour)[0];

      games[games.length - 1].duration_game = hourValue;
    }

    function addUser() {
      const startIndex = line.indexOf('\\');
      const endIndex = line.indexOf('\\t') - 1;
      const charNumber = endIndex - startIndex;
      let user = line.trim().substr(startIndex, charNumber);
      
      user = user.replace(/\\/g, '');
      if (games[games.length - 1].players.indexOf(user) === -1) {
        games[games.length - 1].players.push(user);
      }
    }

    function getKillList() {
      games[games.length - 1].total_kills++;

      const user = listLine[5];
      if (user === '<world>') {
        getKillGlobally();
        return;
      }
      getKillByUser(user);
    }

    function getKillGlobally() {
      const killedBy = listLine.indexOf('killed');

      let killed = listLine[killedBy + 1];
      for (let i = killedBy + 2; i < listLine.length; i++) {
        if (listLine[i] === 'by') {
          break;
        }
        killed += ' ' + listLine[i];
      }

      games[games.length - 1].kills[killed] = games[games.length - 1].kills[killed] - 1;
      return;
    }

    function getKillByUser(user) {
      const reasonFlag = line.match(/(\bMOD\S+\b)/ig).toString();
      const meanOfDeath = meansOfDeathEnum[reasonFlag];

      for (let i = 6; i < listLine.length; i++) {
        if (listLine[i] === 'killed') {
          break;
        }
        user += ' ' + listLine[i];
      }

      games[games.length - 1].kills_by_means[meanOfDeath] = games[games.length - 1].kills[user] + 1 || 1;

      games[games.length - 1].kills[user] = games[games.length - 1].kills[user] + 1 || 1;
      return;
    }

  });
  return games;
}

module.exports = LogParser;
