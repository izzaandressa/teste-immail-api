# teste-immail-api

![picture alt](https://hb.imgix.net/d2148de45bd17c1e5227822f0d154f5d697d23b2.jpg?auto=compress,format&fit=crop&h=353&w=616&s=8f9925aee7b61698db520edfbb9297bc "Quake III Arena")

## Sobre

Esse projeto tem como finalidade parsear um log estático gerado pelo jogo Quake III Arena para um formato JSON retornando via REST Api.

## Para rodar
```
git clone https://gitlab.com/izzaandressa/teste-immail-api.git

cd teste-immail-api

npm install

npm run dev

Espere ou abra http://localhost:4000/
```

## To Do

- [x] npm start or yarn start

- [x] Git

- [x] React Hooks

- [ ] Testes unitários

- [x] Layout responsivo

- [x] Criar Modelo de Dados

- [x] Persistir Dados no MongoDB


## PLUS

- [x] Cloud Mongodb
