const app  = require('../src/app');
const http = require('http');
const port = 4000;

const server = http.createServer(app);

server.listen(port);

console.log(`Servindo na porta ${port}`);
